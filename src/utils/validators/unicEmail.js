import axios from 'axios';

export async function uniqueEmail(value) {
  let sameUsers = [];
  await axios.get('http://localhost:3000/users', {
    params: {
      email: value,
    },
  })
    .then((users) => {
      sameUsers = users.data;
    })
    .catch(() => console.error('Something went wrong please try again later '));
  return !sameUsers.length;
}
