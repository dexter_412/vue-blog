export const validatorText = {
  required: 'Fild is require',
  email: 'Wrong format of email',
  minLength: 'Password too short',
  uniqueNickName: 'Nick name is already used',
  uniqueEmail: 'Email name is already used',
  sameAs: 'Passwords must be same',
};
