import Vue from 'vue';
import VueRouter from 'vue-router';
import AuthGuard from '@/guards/auth-guard';

Vue.use(VueRouter);

export const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/components/pages/PageHome'),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/components/pages/auth/PageLogin'),
    },
    {
      path: '/registration',
      name: 'registration',
      component: () => import('@/components/pages/auth/PageRegistration'),
    },
    {
      path: '/my-profile',
      name: 'my-profile',
      component: () => import('@/components/pages/my-profile/PageMyProfile'),
      beforeEnter: AuthGuard,
    },
    {
      path: '/add-post',
      name: 'add-post',
      component: () => import('@/components/pages/my-profile/PageAddPost'),
      beforeEnter: AuthGuard,
    },
    {
      path: '/post-full/:id',
      name: 'post-full/:id',
      component: () => import('@/components/pages/PageFullPost'),
      props: {},
    },
  ],
});
