export default function (from, to, next) {
  if (sessionStorage.getItem('user_id')) {
    next();
  } else {
    next('/login');
  }
}
