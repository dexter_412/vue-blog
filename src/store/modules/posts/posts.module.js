import axios from 'axios';
import {
  SET_USER_POSTS,
  SET_POSTS,
  SET_CURRENT_POST,
  UPDATE_USER_POSTS,
  REMOVE_DELETED_POST,
  SHOW_ADD_NEW_POST_MODAL,
  HIDE_ADD_NEW_POST_MODAL,
} from './mutatation-types';

export default {
  namespaced: true,

  state: {
    postsList: [],
    userPostList: [],
    currentPost: {},
    addNewPostModal: false,
  },

  mutations: {
    [SET_POSTS](state, posts) {
      state.postsList = posts;
    },

    [SET_USER_POSTS](state, posts) {
      state.userPostList = posts;
    },

    [SET_CURRENT_POST](state, post) {
      state.currentPost = post;
    },

    [UPDATE_USER_POSTS](state, updatedUserPost) {
      const postPosition = state.userPostList.map(post => post.id).indexOf(updatedUserPost.id);
      state.userPostList[postPosition] = updatedUserPost;
      state.userPostList.reverse();
    },

    [REMOVE_DELETED_POST](state, deletedPostId) {
      state.userPostList = state.userPostList.filter(post => post.id !== deletedPostId);
      state.userPostList.reverse();
    },

    [SHOW_ADD_NEW_POST_MODAL](state) {
      state.addNewPostModal = true;
    },

    [HIDE_ADD_NEW_POST_MODAL](state) {
      state.addNewPostModal = false;
    },
  },

  getters: {
    postsHomePage: state => state.postsList.reverse(),

    postsMyProfile: state => state.userPostList.reverse(),
  },

  actions: {
    loadPosts({ commit }) {
      axios.get('http://localhost:3000/posts')
        .then((response) => {
          commit(SET_POSTS, response.data);
        })
        .catch(() => console.error('Something went wrong please try again later '));
    },

    loadUserPosts({ commit }, userID) {
      axios.get('http://localhost:3000/posts', {
        params: {
          authId: userID,
        },
      })
        .then((response) => {
          commit(SET_USER_POSTS, response.data);
        })
        .catch(() => console.error('Something went wrong please try again later '));
    },

    editPost({ commit }, newData) {
      axios.patch(`http://localhost:3000/posts/${newData.id}`, newData)
        .then((newPost) => {
          commit(UPDATE_USER_POSTS, newPost.data);
        })
        .catch(() => console.error('Something went wrong please try again later '));
    },

    deletePost({ commit }, postData) {
      axios.delete(`http://localhost:3000/posts/${postData.id}`)
        .then(() => {
          commit(REMOVE_DELETED_POST, postData.id);
        })
        .catch(() => console.error('Something went wrong please try again later '));
    },

    fullInfoPost({ commit }, postId) {
      axios.get(`http://localhost:3000/posts/${postId}`)
        .then((response) => {
          commit(SET_CURRENT_POST, response.data);
        })
        .catch(() => console.error('Something went wrong please try again later '));
    },

    commentHandler({ commit }, commentInfo) {
      const data = {
        comments: commentInfo.newCommentList,
      };
      axios.patch(`http://localhost:3000/posts/${commentInfo.postId}`, data)
        .then((postInfo) => {
          commit(SET_CURRENT_POST, postInfo.data);
        })
        .catch(() => console.error('Something went wrong please try again later '));
    },

    addNewPost({ commit }, newPostInfo) {
      axios.post('http://localhost:3000/posts', newPostInfo)
        .then(() => {
          commit(SHOW_ADD_NEW_POST_MODAL);
        })
        .catch(() => { console.error('Something went wrong please try again later '); });
    },

    hideAddNewPostModal({ commit }) {
      commit(HIDE_ADD_NEW_POST_MODAL);
    },
  },
};
