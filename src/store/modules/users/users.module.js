import axios from 'axios';
import { router } from '@/router';
import {
  SET_CURRENT_USER,
  SET_LOG_OUT,
  USER_LOGIN_MODAL_SHOW,
  SHOW_SUCCESS_LOGIN_MODAL,
  HIDE_SUCCESS_LOGIN_MODAL,
  SHOW_ERROR_LOGIN_MODAL,
  HIDE_ERROR_LOGIN_MODAL,
  SHOW_SUCCESS_REGISTRATION_MODAL,
  HIDE_SUCCESS_REGISTRATION_MODAL,
} from './mutatation-types';

export default {
  namespaced: true,

  state: {
    currentUser: {},
    isAuth: false,
    successLoginModal: false,
    errorLoginModal: false,
    successRegistrationModal: false,
  },

  mutations: {
    [SET_CURRENT_USER](state, userData) {
      state.currentUser = userData;
      state.isAuth = true;
    },

    [SET_LOG_OUT](state) {
      state.currentUser = {};
      state.isAuth = false;
    },

    [USER_LOGIN_MODAL_SHOW](state) {
      state.userLoginModalShow = true;
    },

    [SHOW_SUCCESS_LOGIN_MODAL](state) {
      state.successLoginModal = true;
    },

    [HIDE_SUCCESS_LOGIN_MODAL](state) {
      state.successLoginModal = false;
      router.push({ name: 'home' });
    },

    [SHOW_ERROR_LOGIN_MODAL](state) {
      state.errorLoginModal = true;
    },

    [HIDE_ERROR_LOGIN_MODAL](state) {
      state.errorLoginModal = false;
    },

    [SHOW_SUCCESS_REGISTRATION_MODAL](state) {
      state.successRegistrationModal = true;
    },

    [HIDE_SUCCESS_REGISTRATION_MODAL](state) {
      state.successRegistrationModal = false;
      router.push({ name: 'login' });
    },
  },

  getters: {
    getAuth: ({ store }) => store.isAuth,
  },

  actions: {
    loadCurrentUser({ commit }, id) {
      axios.get(`http://localhost:3000/users/${id}`)
        .then(response => commit(SET_CURRENT_USER, response.data))
        .catch(() => console.error('Something went wrong please try again later '));
    },

    logOutUser({ commit }) {
      commit(SET_LOG_OUT);
    },

    logInUser({ commit }, userFormData) {
      axios.get('http://localhost:3000/users', {
        params: {
          email: userFormData.email,
          password: userFormData.pass,
        },
      })
        .then((response) => {
          if (response.data.length) {
            sessionStorage.setItem('user_id', response.data[0].id);
            commit(SET_CURRENT_USER, response.data[0]);
            commit(SHOW_SUCCESS_LOGIN_MODAL);
          } else {
            commit(SHOW_ERROR_LOGIN_MODAL);
          }
        })
        .catch(() => console.error('Something went wrong please try again later '));
    },

    hideSuccessLoginModal({ commit }) {
      commit(HIDE_SUCCESS_LOGIN_MODAL);
    },

    hideErrorLoginModal({ commit }) {
      commit(HIDE_ERROR_LOGIN_MODAL);
    },

    regNewUser({ commit }, data) {
      axios.post('http://localhost:3000/users', data)
        .then(() => {
          commit(SHOW_SUCCESS_REGISTRATION_MODAL);
        })
        .catch(() => { console.error('Something went wrong please try again later '); });
    },

    hideSuccessRegistrationModal({ commit }) {
      commit(HIDE_SUCCESS_REGISTRATION_MODAL);
    },
  },
};
