import Vue from 'vue';
import Vuex from 'vuex';
import posts from './modules/posts/posts.module';
import users from './modules/users/users.module';

Vue.use(Vuex);


export default new Vuex.Store({
  modules: {
    posts,
    users,
  },
});
